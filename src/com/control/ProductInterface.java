package com.control;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.model.Product;

@WebService
public interface ProductInterface {
	@WebMethod
	List<Product> getAllProducts();
	@WebMethod
	Product addProduct(String name,String sku,double price);
	@WebMethod
	Product getProductById(long productId);
	@WebMethod
	Product updateProduct(long productId, String name, String sku,double price);
	@WebMethod
	boolean deleteProduct(long productId);
	
	

}