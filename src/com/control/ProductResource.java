package com.control;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.model.Product;
import com.service.ProductService;

@WebService(endpointInterface = "com.control.ProductInterface")
public class ProductResource implements ProductInterface {
	
	private ProductService prodService = new ProductService();

	@Override
	public List<Product> getAllProducts() {
		List<Product> prod = prodService.getAllProducts();
		return prod;
	}

	@Override
	public Product addProduct(String name,String sku,double price) {
		Product product = new Product();
		product.setName(name);
		product.setSku(sku);
		product.setPrice(price);
		return prodService.addProduct(product);
	}

	@Override
	public Product getProductById(@WebParam(partName ="Id")long productId) {
		Product prod = prodService.getProductById(productId);
		return prod;
	}

	@Override
	public Product updateProduct(@WebParam(partName ="Id")long productId,@WebParam(partName = "name")String name,
			@WebParam(partName = "sku")String sku,@WebParam(partName = "price")double price) {
		Product product = new Product();
		product.setId(productId);
		product.setName(name);
		product.setSku(sku);
		product.setPrice(price);
		return prodService.updateProduct(product);
	}

	@Override
	public boolean deleteProduct( @WebParam(partName = "Id")long productId) {
		return prodService.removeProduct(productId);
		
	}

}
