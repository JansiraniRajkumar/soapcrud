package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Product;
import com.util.HibernateUtil;

public class ProductDao {

	public List<Product> getAllProducts() {

		Session session = HibernateUtil.getSession();
		session.beginTransaction();

		List<Product> list = session.createQuery("from Product").list();

		return list;

	}

	public Product insertProduct(Product product) {

		Transaction transaction = null;
		try {
			Session session = HibernateUtil.getSession();

			transaction = session.beginTransaction();
			session.save(product);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return product;

	}

	public Product updateProduct(Product prod) {

		Transaction transaction = null;
		try {
			Session session = HibernateUtil.getSession();

			transaction = session.beginTransaction();
			session.update(prod);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return prod;

	}

	public boolean deleteProduct(long id) {

		Transaction transaction = null;
		Product prod = null;

		try {
			Session session = HibernateUtil.getSession();

			transaction = session.beginTransaction();
			prod = (Product) session.get(Product.class, id);
			session.delete(prod);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return true;

	}

	public Product getProductById(long id) {

		Transaction transaction = null;
		Product product = null;
		try {
			Session session = HibernateUtil.getSession();

			transaction = session.beginTransaction();
			product = (Product) session.get(Product.class, id);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return product;

	}

}
