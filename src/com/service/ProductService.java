package com.service;

import java.util.List;



import com.dao.ProductDao;
import com.model.Product;

public class ProductService {

	private ProductDao prodDao = new ProductDao();

	public List<Product> getAllProducts() {

		return prodDao.getAllProducts();

	}

	public Product getProductById(long id) {
		return prodDao.getProductById(id);
	}

	public Product addProduct(Product product) {
		return prodDao.insertProduct(product);
	}

	public Product updateProduct(Product product) {
		if (product.getId() <= 0) {
			return null;
		}
		return prodDao.updateProduct(product);
	}

	public boolean removeProduct(long id) {
		  return prodDao.deleteProduct(id);
	}

}
